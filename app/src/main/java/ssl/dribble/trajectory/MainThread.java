package ssl.dribble.trajectory;

import lombok.RequiredArgsConstructor;
import ssl.dribble.trajectory.gui.shapes.DrawableCircle;
import ssl.dribble.trajectory.gui.shapes.IDrawableShape;
import ssl.dribble.trajectory.math.ball.BallParameters;
import ssl.dribble.trajectory.math.ball.BallState;
import ssl.dribble.trajectory.math.ball.TrackedBall;
import ssl.dribble.trajectory.math.geometry.Circle;
import ssl.dribble.trajectory.math.geometry.Vector2;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.LockSupport;


@RequiredArgsConstructor
public class MainThread
{
    private static final long LOGIC_FPS = 100;
    private Thread main;
    private boolean running = true;

    private final IDrawableShapesObserver shapesObserver;

    private final BallParameters ballParameters = BallParameters
            .builder()
            .withBallRadius(21.5)
            .withAccRoll(-260)
            .withAccSlide(-3000)
            .withChipDampingZ(0.5)
            .withInertiaDistribution(0.5)
            .withMinHopHeight(10.0)
            .withChipDampingXYOtherHops(0.95)
            .withChipDampingXYFirstHop(0.75)
            .withMaxInterceptableHeight(150)
            .build();

    private WorldFrame worldFrame = new WorldFrame(0, 0, null);

    public void start()
    {
        NamedThreadFactory factory = new NamedThreadFactory("MainLogic");
        long time = System.nanoTime();
        var ballState = BallState.builder()
                .withPos(Vector2.fromXY(200,200))
                .withSpin(Vector2.ZERO_VECTOR)
                .withVel(Vector2.fromXY(2000, 1000))
                .withAcc(Vector2.ZERO_VECTOR)
                .build();
        worldFrame = new WorldFrame(time, time, TrackedBall.fromBallStateVisible(time, ballState)); // initial frame

        main = factory.newThread(this::updateLoop);
        main.start();
    }

    public void update()
    {
        List<IDrawableShape> shapes = new ArrayList<>();

        long time = System.nanoTime();
        double dts = (time - worldFrame.getTimestamp()) * 1e-9;

        var traj = worldFrame.getBall().getTrajectory(ballParameters);
        var ballState = traj.getMilliStateAtTime(dts);

        worldFrame = new WorldFrame(time, worldFrame.getTimestamp(), TrackedBall.fromBallStateVisible(time, ballState));

        shapes.add(new DrawableCircle(Circle.createCircle(worldFrame.getBall().getPos(), 21.5), Color.ORANGE).setFill(true));
        shapes.add(new DrawableCircle(Circle.createCircle(worldFrame.getBall().getPos(), 200), Color.RED));
        shapesObserver.onShapesUpdate(shapes);
    }

    public void updateLoop()
    {
        while (!Thread.interrupted())
        {
            long t0 = System.nanoTime();
            update();
            long t1 = System.nanoTime();
            long sleep = (1_000_000_000L / LOGIC_FPS) - (t1 - t0);
            if (sleep > 0)
            {
                parkNanosSafe(sleep);
            }
        }
    }

    private static void parkNanosSafe(final long sleepTotal)
    {
        final long sleepStart = System.nanoTime();
        long stillSleep = sleepTotal;
        do
        {
            LockSupport.parkNanos(stillSleep);
            long timeSinceStart = System.nanoTime() - sleepStart;
            stillSleep = sleepTotal - timeSinceStart;
        } while (stillSleep > 0);
    }
}
