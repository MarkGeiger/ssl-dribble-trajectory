package ssl.dribble.trajectory.gui;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.io.Serial;


/**
 * Visualization of the field.
 */
public class FieldPanel extends JPanel
{
    @Serial
    private static final long serialVersionUID = 4330620225157027091L;

    private transient Image offImage;


    public void addMouseAdapter(MouseAdapter mouseAdapter)
    {
        addMouseListener(mouseAdapter);
        addMouseMotionListener(mouseAdapter);
        addMouseWheelListener(mouseAdapter);
    }


    public void removeMouseAdapter(MouseAdapter mouseAdapter)
    {
        removeMouseListener(mouseAdapter);
        removeMouseMotionListener(mouseAdapter);
        removeMouseWheelListener(mouseAdapter);
    }


    @Override
    public void paint(final Graphics g1)
    {
        Image image = offImage;
        if (image != null)
        {
            g1.drawImage(image, 0, 0, this);
        } else
        {
            g1.clearRect(0, 0, getWidth(), getHeight());
        }
    }

    public Image getOffImage()
    {
        return offImage;
    }

    public void setOffImage(Image image)
    {
        this.offImage = image;
    }
}
