package ssl.dribble.trajectory.gui;

import lombok.Getter;
import ssl.dribble.trajectory.NamedThreadFactory;
import ssl.dribble.trajectory.gui.shapes.*;
import ssl.dribble.trajectory.math.geometry.*;
import ssl.dribble.trajectory.math.geometry.Rectangle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.concurrent.locks.LockSupport;

public class MainWindow
{
    private static final long VISUALIZATION_FPS = 60;
    private JFrame frame;

    private java.util.List<MouseAdapter> mouseAdapters = java.util.List.of();

    @Getter
    private FieldPane fieldPane = new FieldPane();
    private FieldPanel fieldPanel = new FieldPanel();

    private Image imageBuffer;

    private Thread updateThread;

    public MainWindow()
    {
        initialize();
    }

    private void initialize()
    {
        frame = new JFrame("Main Window");
        frame.setSize(1200, 900);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.add(BorderLayout.CENTER, fieldPanel);

        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                System.exit(0);
            }
        });

        frame.setVisible(true);

        NamedThreadFactory factory = new NamedThreadFactory("VisualizerUpdater");
        updateThread = factory.newThread(this::updateLoop);
        updateThread.start();


        mouseAdapters = java.util.List.of(
                new ZoomMouseAdapter(fieldPane::scale),
                new DragMouseAdapter(fieldPane::drag)
        );

        mouseAdapters.forEach(fieldPanel::addMouseAdapter);
        fieldPane.reset();
    }

    public void update()
    {
        fieldPane.setWidth(fieldPanel.getWidth());
        fieldPane.setHeight(fieldPanel.getHeight());

        /*
         * Drawing only makes sense if we have a valid/existent drawing area because creating an image with size 0 will
         * produce an error. This scenario is possible if the moduli start up before the GUI layouting has been completed
         * and the component size is still 0|0.
         */
        if ((fieldPane.getWidth() < 20) || (fieldPane.getHeight() < 20))
        {
            return;
        }

        if (imageBuffer == null
                || imageBuffer.getHeight(fieldPanel) != fieldPane.getHeight()
                || imageBuffer.getWidth(fieldPanel) != fieldPane.getWidth())
        {
            imageBuffer = fieldPanel.createImage(fieldPane.getWidth(), fieldPane.getHeight());
        }

        Graphics2D g2 = (Graphics2D) imageBuffer.getGraphics();

        java.util.List<IDrawableShape> shapes = new ArrayList<>();
        shapes.add(new DrawableRectangle(Rectangle.createRectangle(Vector2.ZERO_VECTOR, Geometry.getFieldLength(), Geometry.getFieldWidth()))
                .setStrokeWidth(25).setColor(Color.WHITE));
        shapes.add(new DrawableLine(Vector2.fromY(Geometry.getFieldWidth() / 2.0), Vector2.fromY(-Geometry.getFieldWidth() / 2.0))
                .setStrokeWidth(25).setColor(Color.WHITE));
        shapes.add(new DrawableCircle(Circle.createCircle(Vector2.ZERO_VECTOR, 600))
                .setStrokeWidth(25).setColor(Color.WHITE));
        shapes.add(new DrawableLine(Vector2.fromXY(Geometry.getFieldLength() / 2.0 + 200, 900), Vector2.fromXY(Geometry.getFieldLength() / 2.0, 900))
                .setStrokeWidth(40).setColor(Color.YELLOW));
        shapes.add(new DrawableLine(Vector2.fromXY(Geometry.getFieldLength() / 2.0 + 200, -900), Vector2.fromXY(Geometry.getFieldLength() / 2.0, -900))
                .setStrokeWidth(40).setColor(Color.YELLOW));
        shapes.add(new DrawableLine(Vector2.fromXY(Geometry.getFieldLength() / 2.0 + 200, -900), Vector2.fromXY(Geometry.getFieldLength() / 2.0 + 200, 900))
                .setStrokeWidth(40).setColor(Color.YELLOW));
        shapes.add(new DrawableLine(Vector2.fromXY(-Geometry.getFieldLength() / 2.0 - 200, 900), Vector2.fromXY(-Geometry.getFieldLength() / 2.0, 900))
                .setStrokeWidth(40).setColor(Color.BLUE));
        shapes.add(new DrawableLine(Vector2.fromXY(-Geometry.getFieldLength() / 2.0 - 200, -900), Vector2.fromXY(-Geometry.getFieldLength() / 2.0, -900))
                .setStrokeWidth(40).setColor(Color.BLUE));
        shapes.add(new DrawableLine(Vector2.fromXY(-Geometry.getFieldLength() / 2.0 - 200, -900), Vector2.fromXY(-Geometry.getFieldLength() / 2.0 - 200, 900))
                .setStrokeWidth(40).setColor(Color.BLUE));
        shapes.add(new DrawableBotPattern(Vector2.fromXY(2000, 1000), 45, 100, 85, BotID.fromNumber(5)));

        fieldPane.paint(g2, shapes);

        Image currentImage = fieldPanel.getOffImage();
        fieldPanel.setOffImage(imageBuffer);
        imageBuffer = currentImage;

        fieldPanel.repaint();
    }

    public void updateLoop()
    {
        while (!Thread.interrupted())
        {
            long t0 = System.nanoTime();
            update();
            long t1 = System.nanoTime();
            long sleep = (1_000_000_000L / VISUALIZATION_FPS) - (t1 - t0);
            if (sleep > 0)
            {
                parkNanosSafe(sleep);
            }
        }
    }

    private static void parkNanosSafe(final long sleepTotal)
    {
        final long sleepStart = System.nanoTime();
        long stillSleep = sleepTotal;
        do
        {
            LockSupport.parkNanos(stillSleep);
            long timeSinceStart = System.nanoTime() - sleepStart;
            stillSleep = sleepTotal - timeSinceStart;
        } while (stillSleep > 0);
    }
}
