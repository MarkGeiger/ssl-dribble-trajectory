package ssl.dribble.trajectory.gui;

import java.awt.Point;


@FunctionalInterface
public interface FieldScaler
{
    void scale(Point origin, double factor);
}
