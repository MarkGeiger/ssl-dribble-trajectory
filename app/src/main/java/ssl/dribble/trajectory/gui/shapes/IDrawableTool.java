package ssl.dribble.trajectory.gui.shapes;


import ssl.dribble.trajectory.math.geometry.IVector2;

/**
 * Interface for drawing to the FieldPanel.
 */
public interface IDrawableTool
{
	/**
	 * Transform a global point to a GUI point. The color is important to mirror points correctly for the blue AI
	 *
	 * @param globalPosition
	 * @param invert
	 * @return
	 */
	IVector2 transformToGuiCoordinates(IVector2 globalPosition, boolean invert);

	/**
	 * Transform a global angle to a GUI angle.
	 *
	 * @param globalAngle
	 * @param invert
	 * @return
	 */
	double transformToGuiAngle(double globalAngle, boolean invert);

	/**
	 * Scales a global length to a gui length.
	 *
	 * @param length length on field
	 * @return length in gui
	 */
	int scaleGlobalToGui(double length);

	/**
	 * @return scaling factor depending on zoom
	 */
	double getScale();
}
