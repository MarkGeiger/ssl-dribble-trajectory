package ssl.dribble.trajectory.gui.shapes;


import ssl.dribble.trajectory.math.geometry.Circle;
import ssl.dribble.trajectory.math.geometry.IVector2;
import ssl.dribble.trajectory.math.geometry.Lines;
import ssl.dribble.trajectory.math.geometry.Vector2;

import java.awt.*;


/**
 * Outline of a bot with orientation
 */
public class DrawableBot implements IDrawableShape
{
	private final DrawableCircle circle;
	private final DrawableLine line;


	@SuppressWarnings("unused")
	private DrawableBot()
	{
		this(Vector2.ZERO_VECTOR, 0, Color.RED, 90, 75);
	}


	/**
	 * @param pos
	 * @param orientation
	 * @param color
	 * @param radius
	 * @param center2DribbleDist
	 */
	public DrawableBot(final IVector2 pos, final double orientation, final Color color, final double radius,
					   final double center2DribbleDist)
	{
		circle = new DrawableCircle(Circle.createCircle(pos, radius), color);
		line = new DrawableLine(
				Lines.segmentFromPoints(pos, Vector2.fromAngle(orientation).scaleTo(center2DribbleDist)),
				color);
	}


	@Override
	public void paintShape(final Graphics2D g, final IDrawableTool tool, final boolean invert)
	{
		circle.paintShape(g, tool, invert);
		line.paintShape(g, tool, invert);
	}
}
