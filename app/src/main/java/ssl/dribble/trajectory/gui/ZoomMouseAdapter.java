package ssl.dribble.trajectory.gui;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseWheelEvent;

public class ZoomMouseAdapter extends MouseAdapter
{
    private static final double SCROLL_SPEED = 12;

    private final FieldScaler fieldScaler;

    public ZoomMouseAdapter(FieldScaler fieldScaler)
    {
        this.fieldScaler = fieldScaler;
    }


    @Override
    public void mouseWheelMoved(final MouseWheelEvent e)
    {
        final double scroll = 1.0 - e.getWheelRotation() / SCROLL_SPEED;
        fieldScaler.scale(e.getPoint(), scroll);
    }
}
