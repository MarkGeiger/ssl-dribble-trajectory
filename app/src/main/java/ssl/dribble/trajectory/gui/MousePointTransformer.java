package ssl.dribble.trajectory.gui;


import ssl.dribble.trajectory.math.geometry.IVector2;

@FunctionalInterface
public interface MousePointTransformer
{
    IVector2 toGlobal(int dx, int dy);
}
