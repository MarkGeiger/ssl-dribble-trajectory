package ssl.dribble.trajectory.gui;

import org.checkerframework.checker.units.qual.A;
import ssl.dribble.trajectory.IDrawableShapesObserver;
import ssl.dribble.trajectory.gui.shapes.DrawableRectangle;
import ssl.dribble.trajectory.gui.shapes.IDrawableShape;
import ssl.dribble.trajectory.math.geometry.IVector2;
import ssl.dribble.trajectory.math.geometry.Rectangle;
import ssl.dribble.trajectory.math.geometry.Vector2;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class FieldPane implements IDrawableShapesObserver
{
    private static final Color FIELD_COLOR_BACKGROUND = new Color(93, 93, 93);

    private double offsetX;
    private double offsetY;
    private int width;
    private int height;
    private double scale = 1;

    private transient List<IDrawableShape> logicShapes = new ArrayList<>();

    private final FieldTransformation transformation = new FieldTransformation();


    public FieldPane()
    {
        reset();
    }

    public IVector2 getFieldPos(int x, int y)
    {
        return Vector2.fromXY(x - offsetX, y - offsetY - getBorderOffset()).multiply(1f / scale);
    }


    public void scale(Point point, double scroll)
    {
        final double xLen = ((point.x - offsetX) / scale) * 2;
        final double yLen = ((point.y - offsetY) / scale) * 2;

        final double oldLenX = (xLen) * scale;
        final double oldLenY = (yLen) * scale;
        scale *= scroll;
        final double newLenX = (xLen) * scale;
        final double newLenY = (yLen) * scale;
        offsetX -= (newLenX - oldLenX) / 2;
        offsetY -= (newLenY - oldLenY) / 2;
        transformation.setScale(scale);
    }


    public void reset()
    {
        int borderOffset = getBorderOffset();
        scale = transformation.getFieldScale(width, height - borderOffset);
        offsetX = 0;
        offsetY = 0;
    }


    public int getBorderOffset()
    {
        return 0;
    }


    public void drag(int dx, int dy)
    {
        offsetX += dx;
        offsetY += dy;
    }


    public void paint(Graphics2D g2, java.util.List<IDrawableShape> shapes)
    {
        final BasicStroke defaultStroke = new BasicStroke(Math.max(1, transformation.scaleGlobalToGui(10)));
        g2.setColor(FIELD_COLOR_BACKGROUND);
        g2.fillRect(0, 0, width, height);

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        g2.translate(offsetX, offsetY + getBorderOffset());
        g2.scale(scale, scale);

        final Graphics2D gDerived = (Graphics2D) g2.create();
        gDerived.setStroke(defaultStroke);

        DrawableRectangle field = new DrawableRectangle(Rectangle.createRectangle(Vector2.ZERO_VECTOR, transformation.getFieldTotalHeight(), transformation.getFieldTotalWidth()));
        field.setFill(true);
        field.setColor(Color.GREEN.darker());
        field.paintShape(g2, transformation, false);
        field.setFill(false);
        field.setColor(Color.BLACK);
        field.setStrokeWidth(20);
        field.paintShape(g2, transformation, false);

        shapes.forEach(e -> e.paintShape(g2, transformation, false));
        new ArrayList<>(logicShapes).forEach(e -> e.paintShape(g2, transformation, false));

        gDerived.dispose();

        g2.scale(1.0 / scale, 1.0 / scale);
        g2.translate(-offsetX, -offsetY - getBorderOffset());

        g2.setColor(FIELD_COLOR_BACKGROUND);
        g2.fillRect(0, 0, width, getBorderOffset());
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    @Override
    public void onShapesUpdate(List<IDrawableShape> shapes)
    {
        logicShapes = new ArrayList<>(shapes);
    }
}
