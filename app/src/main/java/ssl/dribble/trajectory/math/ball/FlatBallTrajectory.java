package ssl.dribble.trajectory.math.ball;


import lombok.Getter;
import ssl.dribble.trajectory.math.geometry.IVector2;
import ssl.dribble.trajectory.math.geometry.Vector2;


/**
 * Ball trajectory for flat kicks with two phases, separated by a dynamically calculated switch velocity.
 */
public class FlatBallTrajectory extends ABallTrajectory
{
    @Getter
    private final double tSwitch;
    @Getter
    private final IVector2 posSwitch;
    @Getter
    private final IVector2 velSwitch;
    private final IVector2 accSlide;
    private final IVector2 accSlideSpin;
    private final IVector2 accRoll;


    /**
     *
     */
    private FlatBallTrajectory()
    {
        super();

        tSwitch = 0;
        posSwitch = Vector2.ZERO_VECTOR;
        velSwitch = Vector2.ZERO_VECTOR;
        accSlide = Vector2.ZERO_VECTOR;
        accSlideSpin = Vector2.ZERO_VECTOR;
        accRoll = Vector2.ZERO_VECTOR;
    }


    private FlatBallTrajectory(
            final BallParameters parameters,
            final IVector2 initialPos,
            final IVector2 initialVel,
            final IVector2 initialSpin)
    {
        this.parameters = parameters;
        this.initialPos = initialPos;
        this.initialVel = initialVel;
        this.initialSpin = initialSpin;

        // compute relative velocity of ball to ground surface, if ball is rolling this is close to zero
        IVector2 contactVelocity = initialVel.subtractNew(initialSpin.multiplyNew(parameters.getBallRadius()));

        if (contactVelocity.getLength() < 0.01)
        {
            // ball is rolling
            accSlide = initialVel.scaleToNew(parameters.getAccRoll());
            accSlideSpin = accSlide.multiplyNew(1.0 / parameters.getBallRadius());
            tSwitch = 0.0;
        } else
        {
            // ball is sliding
            accSlide = contactVelocity.scaleToNew(parameters.getAccSlide());
            accSlideSpin = accSlide
                    .multiplyNew(1.0 / (parameters.getBallRadius() * parameters.getInertiaDistribution()));
            double f = 1.0 / (1.0 + 1.0 / parameters.getInertiaDistribution());
            IVector2 slideVel = initialSpin.multiplyNew(parameters.getBallRadius()).subtractNew(initialVel).multiply(f);

            if (Math.abs(accSlide.x()) > Math.abs(accSlide.y()))
            {
                tSwitch = slideVel.x() / accSlide.x();
            } else
            {
                tSwitch = slideVel.y() / accSlide.y();
            }
        }

        velSwitch = initialVel.addNew(accSlide.multiplyNew(tSwitch));
        posSwitch = initialPos.addNew(initialVel.multiplyNew(tSwitch)).add(accSlide.multiplyNew(0.5 * tSwitch * tSwitch));

        accRoll = velSwitch.scaleToNew(parameters.getAccRoll());
    }


    /**
     * Create a flat ball trajectory from a ball where kick position/velocity is known.
     *
     * @param parameters
     * @param kickPos    in [mm]
     * @param kickVel    in [mm/s]
     * @param kickSpin   in [rad/s]
     * @return
     */
    public static FlatBallTrajectory fromKick(
            final BallParameters parameters, final IVector2 kickPos, final IVector2 kickVel, final IVector2 kickSpin)
    {
        return new FlatBallTrajectory(parameters, kickPos, kickVel, kickSpin);
    }


    /**
     * Create from state.
     *
     * @param parameters
     * @param posNow     in [mm]
     * @param velNow     in [mm/s]
     * @param spin       in [rad/s]
     * @return
     */
    public static FlatBallTrajectory fromState(
            final BallParameters parameters, final IVector2 posNow, final IVector2 velNow, final IVector2 spin)
    {
        return new FlatBallTrajectory(parameters, posNow, velNow, spin);
    }


    @Override
    public IBallTrajectory withAdjustedInitialPos(final IVector2 posNow, final double time)
    {
        IVector2 deltaPos = posNow.subtractNew(getMilliStateAtTime(time).getPos());

        return new FlatBallTrajectory(parameters, initialPos.addNew(deltaPos), initialVel,
                initialSpin);
    }


    @Override
    public IBallTrajectory withBallParameters(BallParameters ballParameters)
    {
        return new FlatBallTrajectory(ballParameters, initialPos, initialVel, initialSpin);
    }


    @Override
    public BallState getMilliStateAtTime(final double time)
    {
        if (time < 0)
        {
            return BallState.builder()
                    .withPos(initialPos)
                    .withVel(initialVel)
                    .withAcc(Vector2.ZERO_VECTOR)
                    .withSpin(initialSpin)
                    .build();
        }

        if (time < tSwitch)
        {
            IVector2 posNow = initialPos.addNew(initialVel.multiplyNew(time))
                    .add(accSlide.multiplyNew(0.5 * time * time));
            IVector2 velNow = initialVel.addNew(accSlide.multiplyNew(time));
            IVector2 spinNow = initialSpin.subtractNew(accSlideSpin.multiplyNew(time));

            return BallState.builder()
                    .withPos(posNow)
                    .withVel(velNow)
                    .withAcc(accSlide)
                    .withSpin(spinNow)
                    .build();
        }

        double t2 = time - tSwitch;
        if (time > getTimeAtRest())
        {
            t2 = getTimeAtRest() - tSwitch;
        }

        IVector2 posNow = posSwitch.addNew(velSwitch.multiplyNew(t2))
                .add(accRoll.multiplyNew(0.5 * t2 * t2));
        IVector2 velNow = velSwitch.addNew(accRoll.multiplyNew(t2));
        IVector2 spinNow = velNow.multiplyNew(1.0 / parameters.getBallRadius());

        return BallState.builder()
                .withPos(posNow)
                .withVel(velNow)
                .withAcc(accRoll)
                .withSpin(spinNow)
                .build();
    }

    @Override
    public double getTimeAtRest()
    {
        double tStop = -velSwitch.getLength() / parameters.getAccRoll();
        return tSwitch + tStop;
    }


    @Override
    protected double getTimeByDistanceInMillimeters(final double distance)
    {
        double distToSwitch = ((initialVel.getLength() + velSwitch.getLength()) / 2.0) * tSwitch;

        if (distance < distToSwitch)
        {
            // queried distance is in sliding phase
            double v = initialVel.getLength();
            double a = parameters.getAccSlide();
            return (Math.sqrt((v * v) + (2.0 * a * distance)) - v) / a;
        }

        double v = velSwitch.getLength();
        double a = parameters.getAccRoll();
        double tRoll = -v / a;
        double distRoll = (v / 2.0) * tRoll;

        if (distance > (distToSwitch + distRoll))
        {
            // queried distance is beyond total distance
            return Double.POSITIVE_INFINITY;
        }

        // distance is in rolling phase
        double p = distance - distToSwitch;
        double timeToDist = ((Math.sqrt((v * v) + (2.0 * a * p) + 1e-6) - v) / a) + 1e-6;
        if (timeToDist < 1e-3)
        {
            timeToDist = 0.0; // numerical issues...
        }
        assert timeToDist >= 0 : timeToDist;

        return tSwitch + timeToDist;
    }


    @Override
    protected double getTimeByVelocityInMillimetersPerSec(final double velocity)
    {
        if (velocity > initialVel.getLength())
        {
            return 0;
        }

        if (velocity > velSwitch.getLength())
        {
            // requested velocity is during sliding phase
            return -(initialVel.getLength() - velocity) / parameters.getAccSlide();
        }

        // requested velocity is during rolling phase
        double tToVel = -(velSwitch.getLength() - velocity) / parameters.getAccRoll();

        return tSwitch + tToVel;
    }
}
