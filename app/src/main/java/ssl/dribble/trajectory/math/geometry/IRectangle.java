package ssl.dribble.trajectory.math.geometry;


public interface IRectangle
{
    double xExtent();
    double yExtent();

    IVector2 center();
}
