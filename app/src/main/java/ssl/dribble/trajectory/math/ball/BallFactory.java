package ssl.dribble.trajectory.math.ball;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ssl.dribble.trajectory.math.geometry.IVector2;
import ssl.dribble.trajectory.math.geometry.Vector2;


/**
 * Factory class for creating classes for the configured ball models.
 */
@RequiredArgsConstructor
public final class BallFactory
{
	@Getter
	private final BallParameters ballParams;


	public IBallTrajectory createTrajectoryFromState(final BallState state)
	{
		return FlatBallTrajectory
				.fromState(ballParams, state.getPos(), state.getVel(), state.getSpin());
	}


	public IBallTrajectory createTrajectoryFromBallAtRest(final IVector2 pos)
	{
		return FlatBallTrajectory.fromKick(ballParams, pos, Vector2.ZERO_VECTOR, Vector2.ZERO_VECTOR);
	}


	public IBallTrajectory createTrajectoryFromRollingBall(final IVector2 pos, final IVector2 vel)
	{
		IVector2 spin = vel.multiplyNew(1.0 / ballParams.getBallRadius());

		return FlatBallTrajectory.fromKick(ballParams, pos, vel, spin);
	}


	public IBallTrajectory createTrajectoryFromKickedBallWithoutSpin(final IVector2 pos, final IVector2 vel)
	{
		return FlatBallTrajectory.fromKick(ballParams, pos, vel, Vector2.ZERO_VECTOR);
	}


	public IBallTrajectory createTrajectoryFromKickedBall(final IVector2 pos, final IVector2 vel, final IVector2 spin)
	{
		return FlatBallTrajectory.fromKick(ballParams, pos, vel, spin);
	}


	/**
	 * Create a consultant for straight kicks with the default configured implementation
	 *
	 * @return a new ball consultant for straight kicks
	 */
	public IFlatBallConsultant createFlatConsultant()
	{
		return new FlatBallConsultant(ballParams);
	}
}
