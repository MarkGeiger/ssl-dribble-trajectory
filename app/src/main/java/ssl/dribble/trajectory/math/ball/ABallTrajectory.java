package ssl.dribble.trajectory.math.ball;


import lombok.Getter;
import ssl.dribble.trajectory.math.geometry.IVector2;
import ssl.dribble.trajectory.math.geometry.Vector2;

/**
 * Common base implementation for ball trajectories.
 */
public abstract class ABallTrajectory implements IBallTrajectory
{
    /**
     * Parameters for this trajectory.
     */
    @Getter
    protected BallParameters parameters;


    /**
     * Initial position in [mm].
     */
    @Getter
    protected IVector2 initialPos;


    /**
     * Kick velocity in [mm/s].
     */
    protected IVector2 initialVel;


    /**
     * Kick spin in [rad/s].
     */
    @Getter
    protected IVector2 initialSpin;


    /**
     *
     */
    protected ABallTrajectory()
    {
        parameters = BallParameters.builder().build();
        initialPos = Vector2.ZERO_VECTOR;
        initialVel = Vector2.ZERO_VECTOR;
        initialSpin = Vector2.ZERO_VECTOR;
    }


    /**
     * Get the time when the ball comes to rest.
     */
    public abstract double getTimeAtRest();


    /**
     * Get the required time for the ball to travel the given distance.<br>
     * If the distance can not be achieved, the result will be Infinity.
     *
     * @param distance Distance in [mm], must be positive.
     * @return the time in [s] that is need to travel the distance, Inf if the ball stops before reaching the distance.
     */
    protected abstract double getTimeByDistanceInMillimeters(final double distance);


    /**
     * Get the time where the ball reaches a given velocity.<br>
     * If <code>velocity</code> is larger than the current velocity, 0 will be returned
     *
     * @param velocity Velocity in [mm/s], must be positive.
     * @return the time in [s] when the ball's velocity is smaller than or equal to the targetVelocity for the first
     * time.
     */
    protected abstract double getTimeByVelocityInMillimetersPerSec(final double velocity);


    @Override
    public IVector2 getInitialVel()
    {
        return initialVel.multiplyNew(0.001);
    }


    @Override
    public IVector2 getPosByTime(final double time)
    {
        return getMilliStateAtTime(time).getPos();
    }


    @Override
    public IVector2 getVelByTime(final double time)
    {
        return getMilliStateAtTime(time).getVel().multiplyNew(0.001);
    }


    @Override
    public IVector2 getAccByTime(final double time)
    {
        return getMilliStateAtTime(time).getAcc().multiplyNew(0.001);
    }


    @Override
    public IVector2 getSpinByTime(final double time)
    {
        return getMilliStateAtTime(time).getSpin();
    }


    @Override
    public IVector2 getPosByVel(final double targetVelocity)
    {
        if (getAbsVelByTime(0) < targetVelocity)
        {
            return initialPos;
        }

        double time = getTimeByVel(targetVelocity);
        return getPosByTime(time);
    }


    @Override
    public double getTimeByDist(final double travelDistance)
    {
        return getTimeByDistanceInMillimeters(travelDistance);
    }


    @Override
    public double getTimeByVel(final double targetVelocity)
    {
        return getTimeByVelocityInMillimetersPerSec(targetVelocity * 1000.0);
    }


    @Override
    public double getAbsVelByDist(final double distance)
    {
        double time = getTimeByDist(distance);
        return getAbsVelByTime(time);
    }


    @Override
    public double getAbsVelByPos(final IVector2 targetPosition)
    {
        return getAbsVelByDist(initialPos.distanceTo(targetPosition));
    }


    @Override
    public double getTimeByPos(final IVector2 targetPosition)
    {
        return getTimeByDist(initialPos.distanceTo(targetPosition));
    }


    @Override
    public double getDistByTime(final double time)
    {
        return initialPos.distanceTo(getPosByTime(time));
    }


    @Override
    public double getAbsVelByTime(final double time)
    {
        return getMilliStateAtTime(time).getVel().getLength() * 0.001;
    }


    @Override
    public boolean isInterceptableByTime(final double time)
    {
        return false;
    }


    @Override
    public boolean isRollingByTime(final double time)
    {
        return false;
    }
}
