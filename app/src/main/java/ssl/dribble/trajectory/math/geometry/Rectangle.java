package ssl.dribble.trajectory.math.geometry;

public class Rectangle implements IRectangle
{
    private IVector2 center;
    private double xExtent;
    private double yExtent;

    private Rectangle(IVector2 center, double xExtent, double yExtent)
    {
        this.center = center;
        this.xExtent = xExtent;
        this.yExtent = yExtent;
    }

    public static Rectangle createRectangle(IVector2 center, double xExtent, double yExtent)
    {
        return new Rectangle(center, xExtent, yExtent);
    }

    @Override
    public double xExtent()
    {
        return xExtent;
    }

    @Override
    public double yExtent()
    {
        return yExtent;
    }

    @Override
    public IVector2 center()
    {
        return center;
    }
}
