package ssl.dribble.trajectory.math.geometry;

public class Vector2 implements IVector2 {
    private final double x;
    private final double y;

    public static final Vector2 ZERO_VECTOR = new Vector2(0, 0);

    Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static IVector2 fromXY(double x, double y) {
        return new Vector2(x, y);
    }

    public static IVector2 fromX(double x) {
        return new Vector2(x, 0);
    }

    public static IVector2 fromY(double y) {
        return new Vector2(0, y);
    }

    public static IVector2 fromAngle(double angel) {
        return null;
    }

    @Override
    public double x() {
        return x;
    }

    @Override
    public double y() {
        return y;
    }

    @Override
    public IVector2 multiplyNew(double v) {
        return new Vector2(x * v, y * v);
    }

    @Override
    public IVector2 addNew(IVector2 v) {
        return new Vector2(x + v.x(), y + v.y());
    }

    @Override
    public IVector2 subtractNew(IVector2 v) {
        return new Vector2(x - v.x(), y - v.y());
    }

    @Override
    public IVector2 multiply(double v) {
        return new Vector2(x * v, y * v);
    }

    @Override
    public IVector2 add(IVector2 v) {
        return new Vector2(x + v.x(), y + v.y());
    }

    @Override
    public IVector2 turn(double angle) {
        return Vector2.fromXY(
                (x() * Math.cos(angle)) - (y() * Math.sin(angle)),
                (y() * Math.cos(angle)) + (x() * Math.sin(angle)));
    }

    @Override
    public IVector2 scaleTo(double newLength) {
        final double oldLength = getLength2();
        if (oldLength > 1e-3) {
            return multiplyNew(newLength / oldLength);
        }
        // You tried to scale a null-vector to a non-zero length! Vector stays unaffected.
        // but this is normal Math. if vector is zero, result is zero too
        return Vector2.ZERO_VECTOR;
    }

    @Override
    public double getLength() {
        return getLength2();
    }

    @Override
    public double distanceTo(IVector2 targetPosition) {
        return 0;
    }

    @Override
    public IVector2 scaleToNew(double accRoll) {
        return scaleTo(accRoll);
    }

    public double getLength2() {
        return Math.sqrt((x() * x()) + (y() * y()));
    }
}
