package ssl.dribble.trajectory.math.ball;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import ssl.dribble.trajectory.math.geometry.IVector2;
import ssl.dribble.trajectory.math.geometry.Vector2;


/**
 * Data structure for the ball state at a certain time.
 * <br>
 * <b>WARNING: Units of this class are [mm], [mm/s] ([rad/s]), [mm/s^2] !!!</b>
 */
@Value
@Builder(setterPrefix = "with", toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class BallState {
    /**
     * Position in [mm]
     */
    @NonNull
    IVector2 pos;

    /**
     * Velocity in [mm/s]
     */
    @NonNull
    IVector2 vel;

    /**
     * Acceleration in [mm/s^2]
     */
    @NonNull
    IVector2 acc;

    /**
     * The spin of the ball in [rad/s], positive spin corresponds to positive linear velocity
     */
    IVector2 spin;


    public BallState() {
        pos = Vector2.ZERO_VECTOR;
        vel = Vector2.ZERO_VECTOR;
        acc = Vector2.ZERO_VECTOR;
        spin = Vector2.ZERO_VECTOR;
    }

    /**
     * Is this ball rolling based on its spin and velocity?
     *
     * @param ballRadius in [mm]
     * @return
     */
    public boolean isRolling(double ballRadius) {
        // compute relative velocity of ball to ground surface, if ball is rolling this is close to zero
        IVector2 contactVelocity = vel.subtractNew(spin.multiplyNew(ballRadius));

        return contactVelocity.getLength() < 0.01;
    }
}
