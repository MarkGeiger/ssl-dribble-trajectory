package ssl.dribble.trajectory.math.geometry;

public interface ILineSegment
{
    IVector2 getPathStart();

    IVector2 getPathEnd();
}
