package ssl.dribble.trajectory.math.geometry;

public interface ICircle
{
    double radius();

    IVector2 center();
}
