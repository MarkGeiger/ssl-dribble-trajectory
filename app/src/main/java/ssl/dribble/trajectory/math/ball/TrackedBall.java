package ssl.dribble.trajectory.math.ball;

import ssl.dribble.trajectory.math.geometry.IVector2;

import java.util.Arrays;
import java.util.List;

/**
 * A tracked (filtered, predicted) ball.
 */
public class TrackedBall
{
    private final long timestamp;
    private final BallState state;
    private final long lastVisibleTimestamp;

    // cache some fields on demand to increase performance
    private IVector2 pos;
    private IVector2 vel;
    private IBallTrajectory ballTrajectory;
    private IFlatBallConsultant straightBallConsultant;


    /**
     * Create an empty tracked ball.
     */
    private TrackedBall()
    {
        state = new BallState();
        lastVisibleTimestamp = 0;
        timestamp = 0;
    }


    private TrackedBall(final long timestamp, final BallState state, final long lastVisibleTimestamp)
    {
        this.timestamp = timestamp;
        this.state = state;
        this.lastVisibleTimestamp = lastVisibleTimestamp;
    }


    /**
     * @return an empty tracked ball
     */
    public static TrackedBall createStub()
    {
        return new TrackedBall();
    }


    public static TrackedBall fromBallStateVisible(final long timestamp, final BallState state)
    {
        return new TrackedBall(timestamp, state, timestamp);
    }


    public long getTimestamp()
    {
        return timestamp;
    }


    public IBallTrajectory getTrajectory(BallParameters ballParams)
    {
        if (ballTrajectory == null)
        {
            ballTrajectory = FlatBallTrajectory.fromState(ballParams, state.getPos(), state.getVel(), getState().getSpin());
        }
        return ballTrajectory;
    }


    public IVector2 getPos()
    {
        if (pos == null)
        {
            pos = getPos3();
        }
        return pos;
    }


    public IVector2 getVel()
    {
        if (vel == null)
        {
            vel = getVel3();
        }
        return vel;
    }


    public IVector2 getAcc()
    {
        return getAcc3();
    }


    public IVector2 getPos3()
    {
        return state.getPos();
    }


    public IVector2 getVel3()
    {
        return state.getVel().multiplyNew(0.001);
    }


    public IVector2 getAcc3()
    {
        return state.getAcc().multiplyNew(0.001);
    }


    public double invisibleFor()
    {
        return (getTimestamp() - lastVisibleTimestamp) * 1e-9;
    }


    public boolean isOnCam(final double seconds)
    {
        return invisibleFor() <= seconds;
    }


    public IFlatBallConsultant getStraightConsultant(BallParameters ballParams)
    {
        if (straightBallConsultant == null)
        {
            straightBallConsultant = new FlatBallConsultant(ballParams);
        }
        return straightBallConsultant;
    }


    public long getLastVisibleTimestamp()
    {
        return lastVisibleTimestamp;
    }


    public BallState getState()
    {
        return state;
    }


    public double getQuality()
    {
        final double invisibleTime = (timestamp - lastVisibleTimestamp) / 1e9;
        return 1 - Math.min(1, invisibleTime / 0.2);
    }


    public List<String> getHeaders()
    {
        return Arrays.asList("timestamp", "pos_x", "pos_y", "pos_z", "vel_x", "vel_y", "vel_z", "acc_x", "acc_y", "acc_z",
                "lastVisibleTimestamp", "chipped");
    }
}