package ssl.dribble.trajectory.math.ball;



/**
 * Consultant for straight kicked balls.
 */
public interface IFlatBallConsultant
{
	/**
	 * Get the initial velocity such that the ball has the given velocity after given time
	 *
	 * @param endVel [m/s]
	 * @param time   [s]
	 * @return initial velocity [m/s]
	 */
	double getInitVelForTime(final double endVel, final double time);


	/**
	 * Get the initial velocity such that the ball has travelled the given distance after given time
	 *
	 * @param distance [mm]
	 * @param time     [s]
	 * @return initial velocity [m/s]
	 */
	default double getInitVelForTimeDist(final double distance, final double time)
	{
		return 0;
	}


	/**
	 * Get the initial velocity such that the ball travels the given distance and has the given end velocity.
	 *
	 * @param distance [mm]
	 * @param endVel   [m/s]
	 * @return initial velocity [m/s]
	 */
	double getInitVelForDist(final double distance, final double endVel);
}
