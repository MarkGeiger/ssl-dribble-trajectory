package ssl.dribble.trajectory.math.geometry;

public class Geometry
{
    private Geometry()
    {

    }

    public static double getFieldWidth()
    {
        return 9000;
    }

    public static double getFieldLength()
    {
        return 12000;
    }
}
