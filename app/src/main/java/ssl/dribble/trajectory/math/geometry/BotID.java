package ssl.dribble.trajectory.math.geometry;

import java.awt.*;

public class BotID
{
    private int number;

    private BotID(int number)
    {
        this.number = number;
    }

    public static BotID noBot()
    {
        return new BotID(0);
    }

    public static BotID fromNumber(int i)
    {
        return new BotID(i);
    }

    public int getNumber()
    {
        return number;
    }

    public Color getColor()
    {
        return Color.YELLOW;
    }
}
