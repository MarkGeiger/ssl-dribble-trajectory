package ssl.dribble.trajectory.math.geometry;

public class Lines
{
    public static class LineSegment implements ILineSegment
    {
        private IVector2 start;
        private IVector2 end;

        LineSegment(IVector2 start, IVector2 end)
        {
            this.start = start;
            this.end = end;
        }

        @Override
        public IVector2 getPathStart()
        {
            return start;
        }

        @Override
        public IVector2 getPathEnd()
        {
            return end;
        }
    }

    public static ILineSegment segmentFromPoints(IVector2 start, IVector2 end)
    {
        return new LineSegment(start, end);
    }
}
