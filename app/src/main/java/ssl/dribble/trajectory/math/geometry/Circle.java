package ssl.dribble.trajectory.math.geometry;

public class Circle implements ICircle
{
    private IVector2 center;
    private double radius;

    private Circle(IVector2 center, double radius)
    {
        this.center = center;
        this.radius = radius;
    }

    public static ICircle createCircle(IVector2 v, double radius)
    {
        return new Circle(v, radius);
    }

    @Override
    public double radius()
    {
        return radius;
    }

    @Override
    public IVector2 center()
    {
        return center;
    }
}
