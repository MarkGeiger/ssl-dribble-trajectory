package ssl.dribble.trajectory.math.geometry;

public interface IVector2 {
    double x();

    double y();

    IVector2 multiplyNew(double v);

    IVector2 addNew(IVector2 v);

    IVector2 subtractNew(IVector2 v);

    IVector2 multiply(double v);

    IVector2 add(IVector2 v);

    IVector2 turn(double angle);

    IVector2 scaleTo(double dist);

    double getLength();

    double distanceTo(IVector2 targetPosition);

    IVector2 scaleToNew(double accRoll);
}
