package ssl.dribble.trajectory;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import ssl.dribble.trajectory.math.ball.TrackedBall;


@Value
@RequiredArgsConstructor
public class WorldFrame
{
    long timestamp;
    long oldTimestamp;

    TrackedBall ball;
}
