package ssl.dribble.trajectory;

import ssl.dribble.trajectory.gui.MainWindow;

public class App
{
    public static void main(String[] args)
    {
        MainWindow mainWindow = new MainWindow();
        MainThread thread = new MainThread(mainWindow.getFieldPane());
        thread.start();
    }
}
