package ssl.dribble.trajectory;

import ssl.dribble.trajectory.gui.shapes.IDrawableShape;

import java.util.List;

public interface IDrawableShapesObserver
{
    void onShapesUpdate(List<IDrawableShape> shapes);
}
